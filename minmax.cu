#include "timer.h"

#if defined(_WIN32)
#pragma warning(push)
#pragma warning(disable: 4267)
#endif
#include <thrust/device_vector.h>
#include <thrust/host_vector.h>
#if defined(_WIN32)
#pragma warning(pop)
#endif

#include <curand.h>

#include <cuda_runtime.h>
#include <device_launch_parameters.h>
#include <device_types.h>
#include <math_functions.h>
#include <omp.h>

const int THREADS_COUNT = 128;
const int ITERATIONS = 10;

typedef void (*GpuComputeMinMaxFunction)(const float*, int);



__device__ float minV;
__device__ float maxV;

// Simple computation fetch values directly from gmem,
// atomically writing the result to minV and maxV
__global__
void computeMinMaxNoSM(const float* values) {
  if (threadIdx.x == 0) {
    float m, M;
    m = M = values[blockIdx.x * blockDim.x];
    for (int i = 1; i < blockDim.x; ++i) {
      float v = values[i + blockIdx.x * blockDim.x];
      m = min(m, v);
      M = max(M, v);
    }
    atomicMin((unsigned int*)&minV, (unsigned int)__float_as_int(m));
    atomicMax((unsigned int*)&maxV, (unsigned int)__float_as_int(M));
  }
}

// First store all the block related values in cache.
// Then only the first thread of the block will compute the min max.
// This takes advantage of coalescing will reading the values
// and reduce contention on minV and maxV memory write accesses (a single write per block).
__global__
void computeMinMaxSM(const float* values) {
  __shared__ float cachedValues[THREADS_COUNT];

  cachedValues[threadIdx.x] = values[threadIdx.x + blockIdx.x * blockDim.x];

  __syncthreads();

  if (threadIdx.x == 0) {
    float m, M;
    m = M = cachedValues[0];
    for (int i = 1; i < blockDim.x; ++i) {
      m = min(m, cachedValues[i]);
      M = max(M, cachedValues[i]);
    }
    atomicMin((unsigned int*)&minV, (unsigned int)__float_as_int(m));
    atomicMax((unsigned int*)&maxV, (unsigned int)__float_as_int(M));
  }
}

// Coalescing read.
// Compute block min max by reducing the value set by to each step.
// Only works with power of two count of values.
// - ABCDEFGH
//   => minmax A=(AB) C=(CD) E=(EF) G=(GH)
//   => minmax A=(AC) E=(EG)
//   => minmax (AE)
// Reduce contention on minV and maxV memory write accesses (a single write per block).
// => comparison of remaining values must be done using previous methods.
__global__
void computeMinMaxSM2(const float* values) {
  __shared__ float2 minmax[THREADS_COUNT];

  minmax[threadIdx.x].x =
      minmax[threadIdx.x].y = values[threadIdx.x + blockIdx.x * blockDim.x];

  __syncthreads();
  int itCount = log2(float(THREADS_COUNT)) - 1;
#pragma unroll
  for (int i = 0; i < itCount; ++i) {
    int offset = 1 << i;
    int mask = (offset << 1) - 1;
    if ((threadIdx.x & mask) == 0) {
       minmax[threadIdx.x] = make_float2(
         min(minmax[threadIdx.x].x, minmax[threadIdx.x + offset].x),
         max(minmax[threadIdx.x].y, minmax[threadIdx.x + offset].y));
    }
  }

  if (threadIdx.x == 0) {
    float m = min(minmax[0].x, minmax[threadIdx.x + THREADS_COUNT / 2].x);
    float M = max(minmax[0].y, minmax[threadIdx.x + THREADS_COUNT / 2].y);
    atomicMin((unsigned int*)&minV, (unsigned int)__float_as_int(m));
    atomicMax((unsigned int*)&maxV, (unsigned int)__float_as_int(M));
  }
}

// Coalescing read with float4 instead of float reading 4 times the number of
// values processed by previous SM2.
// Compute block min max by reducing the value set by to each step.
// Only works with power of two count of values containing at least for values.
// - ABCDEFGH
//   => minmax A=(AB) C=(CD) E=(EF) G=(GH)
//   => minmax A=(AC) E=(EG)
//   => minmax (AE)
// Reduce contention on minV and maxV memory write accesses (a single write per block).
// => comparison of remaining values must be done using previous methods.
__global__
void computeMinMaxSM3(const float4* values) {
  __shared__ float2 minmax[THREADS_COUNT];

  float4 v = values[threadIdx.x + blockIdx.x * blockDim.x];
  minmax[threadIdx.x].x =
      min(v.x, min(v.y, min(v.z, v.w)));
  minmax[threadIdx.x].y =
      max(v.x, max(v.y, max(v.z, v.w)));

  __syncthreads();
  int itCount = log2(float(THREADS_COUNT)) - 1;
#pragma unroll
  for (int i = 0; i < itCount; ++i) {
    int offset = 1 << i;
    int mask = (offset << 1) - 1;
    if ((threadIdx.x & mask) == 0) {
       minmax[threadIdx.x] = make_float2(
         min(minmax[threadIdx.x].x, minmax[threadIdx.x + offset].x),
         max(minmax[threadIdx.x].y, minmax[threadIdx.x + offset].y));
    }
  }

  if (threadIdx.x == 0) {
    float m = min(minmax[0].x, minmax[threadIdx.x + THREADS_COUNT / 2].x);
    float M = max(minmax[0].y, minmax[threadIdx.x + THREADS_COUNT / 2].y);
    atomicMin((unsigned int*)&minV, (unsigned int)__float_as_int(m));
    atomicMax((unsigned int*)&maxV, (unsigned int)__float_as_int(M));
  }
}

// Coalescing read with float4. Same as SM3.
// Compute block min max by reducing the value set by to each step.
// Only works with power of two count of values containing at least for values.
// - ABCDEFGH
//   => minmax A=(AB) C=(CD) E=(EF) G=(GH)
//   => minmax A=(AC) E=(EG)
//   => minmax (AE)
// Same
// Try harder to reduce write contention by storing a minmax per block.
// A second kernel is in charge of reducing this new set of minmaxs.
// => comparison of remaining values must be done using previous methods.
__global__
void computeMinMaxSM3PerBlock(const float4* values, float2* perBlockMinMax) {
  __shared__ float2 minmax[THREADS_COUNT];

  float4 v = values[threadIdx.x + blockIdx.x * blockDim.x];
  minmax[threadIdx.x].x =
      min(v.x, min(v.y, min(v.z, v.w)));
  minmax[threadIdx.x].y =
      max(v.x, max(v.y, max(v.z, v.w)));

  __syncthreads();
  int itCount = log2(float(THREADS_COUNT)) - 1;
#pragma unroll
  for (int i = 0; i < itCount; ++i) {
    int offset = 1 << i;
    int mask = (offset << 1) - 1;
    if ((threadIdx.x & mask) == 0) {
       minmax[threadIdx.x] = make_float2(
         min(minmax[threadIdx.x].x, minmax[threadIdx.x + offset].x),
         max(minmax[threadIdx.x].y, minmax[threadIdx.x + offset].y));
    }
  }

  if (threadIdx.x == 0) {
   perBlockMinMax[blockIdx.x] = make_float2(
     min(minmax[0].x, minmax[threadIdx.x + THREADS_COUNT / 2].x),
     max(minmax[0].y, minmax[threadIdx.x + THREADS_COUNT / 2].y));
  }
}

void gpuComputeMinMaxNoSM(const float* values, int size) {
  const int BLOCKS_COUNT = size / THREADS_COUNT;
  const int TAIL_THREADS_COUNT = size % THREADS_COUNT;
  const int MAX_BLOCKS_PER_CALL = 65535;

  int offset = 0;
  for (int i = 0; i < BLOCKS_COUNT / MAX_BLOCKS_PER_CALL; ++i) {
    computeMinMaxNoSM<<<MAX_BLOCKS_PER_CALL, THREADS_COUNT>>>(values + offset);
    offset += MAX_BLOCKS_PER_CALL * THREADS_COUNT;
  }
  int remainingBlocks = BLOCKS_COUNT % MAX_BLOCKS_PER_CALL;
  if (remainingBlocks) {
    computeMinMaxNoSM<<<remainingBlocks, THREADS_COUNT>>>(values + offset);
    offset += remainingBlocks * THREADS_COUNT;
  }
  if (TAIL_THREADS_COUNT)
    computeMinMaxNoSM<<<1, TAIL_THREADS_COUNT>>>(values + offset);
}

void gpuComputeMinMaxSM(const float* values, int size) {
  const int BLOCKS_COUNT = size / THREADS_COUNT;
  const int TAIL_THREADS_COUNT = size % THREADS_COUNT;
  const int MAX_BLOCKS_PER_CALL = 65535;

  int offset = 0;
  for (int i = 0; i < BLOCKS_COUNT / MAX_BLOCKS_PER_CALL; ++i) {
    computeMinMaxSM<<<MAX_BLOCKS_PER_CALL, THREADS_COUNT>>>(values + offset);
    offset += MAX_BLOCKS_PER_CALL * THREADS_COUNT;
  }
  int remainingBlocks = BLOCKS_COUNT % MAX_BLOCKS_PER_CALL;
  if (remainingBlocks) {
    computeMinMaxSM<<<remainingBlocks, THREADS_COUNT>>>(values + offset);
    offset += remainingBlocks * THREADS_COUNT;
  }
  if (TAIL_THREADS_COUNT)
    computeMinMaxSM<<<1, TAIL_THREADS_COUNT>>>(values + offset);
}

void gpuComputeMinMaxSM2(const float* values, int size) {
  const int BLOCKS_COUNT = size / THREADS_COUNT;
  const int TAIL_THREADS_COUNT = size % THREADS_COUNT;
  const int MAX_BLOCKS_PER_CALL = 65535;

  int offset = 0;
  for (int i = 0; i < BLOCKS_COUNT / MAX_BLOCKS_PER_CALL; ++i) {
    computeMinMaxSM2<<<MAX_BLOCKS_PER_CALL, THREADS_COUNT>>>(values + offset);
    offset += MAX_BLOCKS_PER_CALL * THREADS_COUNT;
  }
  int remainingBlocks = BLOCKS_COUNT % MAX_BLOCKS_PER_CALL;
  if (remainingBlocks) {
    computeMinMaxSM2<<<remainingBlocks, THREADS_COUNT>>>(values + offset);
    offset += remainingBlocks * THREADS_COUNT;
  }
  if (TAIL_THREADS_COUNT)
    computeMinMaxSM<<<1, TAIL_THREADS_COUNT>>>(values + offset);
}

void gpuComputeMinMaxSM3(const float* values, int size) {
  const int BLOCKS_COUNT = (size / 4) / THREADS_COUNT;
  const int TAIL_THREADS_COUNT = size % THREADS_COUNT;
  const int MAX_BLOCKS_PER_CALL = 65535;

  int offset = 0;
  for (int i = 0; i < BLOCKS_COUNT / MAX_BLOCKS_PER_CALL; ++i) {
    computeMinMaxSM3<<<MAX_BLOCKS_PER_CALL, THREADS_COUNT>>>(
        (float4*)(values + offset));
    offset += MAX_BLOCKS_PER_CALL * THREADS_COUNT;
  }
  int remainingBlocks = BLOCKS_COUNT % MAX_BLOCKS_PER_CALL;
  if (remainingBlocks) {
    computeMinMaxSM3<<<remainingBlocks, THREADS_COUNT>>>(
        (float4*)(values + offset));
    offset += remainingBlocks * THREADS_COUNT;
  }
  if (TAIL_THREADS_COUNT)
    computeMinMaxSM<<<1, TAIL_THREADS_COUNT>>>(values + offset);
}

void gpuComputeMinMaxSM3PerBlock(const float* values, int size) {
  const int BLOCKS_COUNT = (size / 4) / THREADS_COUNT;
  const int TAIL_THREADS_COUNT = size % THREADS_COUNT;
  const int MAX_BLOCKS_PER_CALL = 65535;

  thrust::device_vector<float2> perBlockMinMax(MAX_BLOCKS_PER_CALL);

  int offset = 0;
  for (int i = 0; i < BLOCKS_COUNT / MAX_BLOCKS_PER_CALL; ++i) {
    computeMinMaxSM3PerBlock<<<MAX_BLOCKS_PER_CALL, THREADS_COUNT>>>(
        (float4*)(values + offset),
        thrust::raw_pointer_cast(perBlockMinMax.data()));
    gpuComputeMinMaxSM3((float*)thrust::raw_pointer_cast(perBlockMinMax.data()),
                        2 * MAX_BLOCKS_PER_CALL);
    offset += MAX_BLOCKS_PER_CALL * THREADS_COUNT;
  }
  int remainingBlocks = BLOCKS_COUNT % MAX_BLOCKS_PER_CALL;
  if (remainingBlocks) {
    computeMinMaxSM3PerBlock<<<remainingBlocks, THREADS_COUNT>>>(
        (float4*)(values + offset),
        thrust::raw_pointer_cast(perBlockMinMax.data()));
    gpuComputeMinMaxSM3((float*)thrust::raw_pointer_cast(perBlockMinMax.data()),
                        2 * remainingBlocks);
    offset += remainingBlocks * THREADS_COUNT;
  }
  if (TAIL_THREADS_COUNT)
    computeMinMaxSM<<<1, TAIL_THREADS_COUNT>>>(values + offset);
}

void gpuComputeMinMax(void method(const float* values, int size), const thrust::device_vector<float>& values, float& m, float& M) {
  m = std::numeric_limits<float>::max();
  M = 0;

  float* deviceMaxV=0, *deviceMinV=0;
  cudaGetSymbolAddress((void**)&deviceMinV, minV);
  cudaGetSymbolAddress((void**)&deviceMaxV, maxV);

  cudaMemcpy(deviceMinV, &m, sizeof(m), cudaMemcpyHostToDevice);
  cudaMemcpy(deviceMaxV, &M, sizeof(M), cudaMemcpyHostToDevice);

  method(thrust::raw_pointer_cast(values.data()), values.size());

  cudaMemcpy(&m, deviceMinV, sizeof(m), cudaMemcpyDeviceToHost);
  cudaMemcpy(&M, deviceMaxV, sizeof(M), cudaMemcpyDeviceToHost);
}

void cpuComputeMinMax(const thrust::host_vector<float>& values, float& m, float& M) {
  m = values[0], M = m;
#if !defined(_WIN32)
#pragma omp parallel for reduction(min: m) reduction(max: M)
  for (int i = 1; i < values.size(); ++i) {
    m = std::min(m, values[i]);
    M = std::max(M, values[i]);
  }
#else
#pragma omp parallel
  {
    float lm = m, lM = M;
#pragma omp for
    for (int i = 0; i < values.size(); ++i) {
      lm = (std::min)(lm, values[i]);
      lM = (std::max)(lM, values[i]);
    }
#pragma omp critical
    {
      m = (std::min)(m, lm);
      M = (std::max)(M, lM);
    }
  }
#endif
}

void timeCpu(const thrust::host_vector<float>& values) {
  float m, M;
  StartTimer();
  for (int i = 0; i < ITERATIONS; ++i) {
    cpuComputeMinMax(values, m, M);
  }
  double seconds = GetTimer();
  std::cout << "time(" << ITERATIONS << " iterations): " << seconds << "ms" << std::endl;
}


void timeGpu(const std::string& name, GpuComputeMinMaxFunction function,
             const thrust::device_vector<float>& values) {
  float m, M;
  std::cout << "timing " << name << std::endl;
  StartTimer();
  for (int i = 0; i < ITERATIONS; ++i) {
    gpuComputeMinMax(function, values, m, M);
  }
  cudaDeviceSynchronize();
  double seconds = GetTimer();

  std::cout << "time(" << ITERATIONS << " iterations): " << seconds << "ms" << std::endl;
}

int main(int argc, char** argv) {
  thrust::device_vector<float> values(512 * 1024 * 1024 / 4);

  curandGenerator_t generator;
  curandCreateGenerator(&generator, CURAND_RNG_PSEUDO_DEFAULT);
  curandSetPseudoRandomGeneratorSeed(generator, 55);
  curandGenerateUniform(generator, thrust::raw_pointer_cast(values.data()), values.size());

  // Cpu computed min max
  {
    float m, M;
    cpuComputeMinMax(values, m, M);
    std::cout << "cpu: min " << m << ", max " << M << std::endl;
    timeCpu(values);
  }

  {
    struct Method {
      Method(const char* name, GpuComputeMinMaxFunction method)
          : name(name), function(function) {
      }
      const char* name;
      GpuComputeMinMaxFunction function;
    } methods[] = {
      Method("gpuComputeMinMaxNoSM", &gpuComputeMinMaxNoSM),
      Method("gpuComputeMinMaxSM", &gpuComputeMinMaxSM),
      Method("gpuComputeMinMaxSM2", &gpuComputeMinMaxSM2),
      Method("gpuComputeMinMaxSM3", &gpuComputeMinMaxSM3),
      Method("gpuComputeMinMaxSM3PerBlock", &gpuComputeMinMaxSM3PerBlock)
    };

    float m, M;
    gpuComputeMinMax(gpuComputeMinMaxNoSM, values, m, M);
    std::cout << "gpu: gpuComputeMinMaxNoSM: min " << m << ", max " << M << std::endl;
    gpuComputeMinMax(gpuComputeMinMaxSM, values, m, M);
    std::cout << "gpu: gpuComputeMinMaxSM: min " << m << ", max " << M << std::endl;
    gpuComputeMinMax(gpuComputeMinMaxSM2, values, m, M);
    std::cout << "gpu: gpuComputeMinMaxSM2: min " << m << ", max " << M << std::endl;
    gpuComputeMinMax(gpuComputeMinMaxSM3, values, m, M);
    std::cout << "gpu: gpuComputeMinMaxSM3: min " << m << ", max " << M << std::endl;
    gpuComputeMinMax(gpuComputeMinMaxSM3PerBlock, values, m, M);
    std::cout << "gpu: gpuComputeMinMaxSM3PerBlock: min " << m << ", max " << M << std::endl;


    timeGpu("gpuComputeMinMaxNoSM", gpuComputeMinMaxNoSM, values);
    timeGpu("gpuComputeMinMaxSM", gpuComputeMinMaxSM, values);
    timeGpu("gpuComputeMinMaxSM2", gpuComputeMinMaxSM2, values);
    timeGpu("gpuComputeMinMaxSM3", gpuComputeMinMaxSM3, values);
    timeGpu("gpuComputeMinMaxSM3PerBlock", gpuComputeMinMaxSM3PerBlock, values);

  }

  return 0;
}
